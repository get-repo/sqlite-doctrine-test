<?php

namespace GetRepo\SqliteDoctrineTest;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Kernel;

abstract class AbstractKernel extends Kernel
{
    use MicroKernelTrait;

    public function getSqliteDbPath(): string
    {
        return sprintf('%s/sqlite/%s.db', sys_get_temp_dir(), uniqid('test'));
    }

    public function registerBundles(): iterable
    {
        return array_merge(
            [
                new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
                new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            ],
            $this->registeredBundles()
        );
    }

    protected function configureContainer(ContainerBuilder $builder, LoaderInterface $loader): void
    {
        $dbPath = $this->getSqliteDbPath();
        $filesystem = new Filesystem();
        $filesystem->mkdir(dirname($dbPath));
        if (file_exists($dbPath)) {
            unlink($dbPath);
        }

        // FRAMEWORK CONFIG
        $builder->loadFromExtension('framework', [
            'secret' => 'TEST',
            'test' => true,
        ]);

        // DOCTRINE CONFIG
        $builder->loadFromExtension('doctrine', [
            'dbal' => [
                'default_connection' => 'test',
                'connections' => [
                    'test' => [
                        'driver' => 'pdo_sqlite',
                        'path' => $dbPath,
                    ],
                ],
            ],
            'orm' => $this->getDoctrineORMConfiguration(),
        ]);

        $this->registeredContainerConfiguration($builder, $loader);
    }

    protected function registeredBundles(): array
    {
        // override this method
        return [];
    }

    protected function registeredContainerConfiguration(ContainerBuilder $builder, LoaderInterface $loader): void
    {
        // override this method
    }

    protected function getDoctrineORMConfiguration(): array
    {
        // override this method to return doctrine ORM configuration
        return [];
    }
}

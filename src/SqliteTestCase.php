<?php

namespace GetRepo\SqliteDoctrineTest;

use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;

abstract class SqliteTestCase extends WebTestCase
{
    private static ?ManagerRegistry $doctrine = null;
    private static ?AbstractBrowser $client = null;

    public static function getDoctrine(): ManagerRegistry
    {
        if (!self::$doctrine) {
            self::buildKernel();
        }

        return self::$doctrine; // @phpstan-ignore-line
    }

    protected static function getClient(AbstractBrowser $newClient = null): ?AbstractBrowser
    {
        if (!self::$client) {
            self::buildKernel();
        }

        return self::$client;
    }

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::buildKernel();
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        self::db('drop');
    }

    protected static function isDebug(): bool
    {
        return \in_array('--debug', $_SERVER['argv'] ?? []);
    }

    /**
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    private static function buildKernel(): void
    {
        if (!static::$kernel) {
            $kernelClass = getenv('KERNEL_CLASS');
            if (!$kernelClass) {
                echo 'Environment variable KERNEL_CLASS was not found.' . PHP_EOL;
                echo 'Maybe you forgot to add it phpunit.xml ?' . PHP_EOL;
                exit(1);
            }
            if (!class_exists($kernelClass)) {
                echo "Kernel class KERNEL_CLASS = {$kernelClass} does not exists." . PHP_EOL;
                exit(1);
            }
            parent::setUpBeforeClass();

            // createClient will create the $kernel
            self::$client = self::createClient(['debug' => self::isDebug()]);
            /** @var AbstractKernel $kernel */
            $kernel = self::$kernel;
            self::$doctrine = $kernel->getContainer()->get('doctrine'); // @phpstan-ignore-line
            self::db('create');
        }
    }

    private static function db(string $action): void
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = self::getDoctrine()->getManager();
        $connection = $em->getConnection();
        $params = $connection->getParams();
        /** @var AbstractKernel $kernel */
        $kernel = self::$kernel;
        $dbPath = $params['path'] ?? $kernel->getSqliteDbPath();

        $schemaManager = $connection->createSchemaManager();

        switch ($action) {
            case 'drop':
                $schemaManager->dropDatabase($dbPath);
                break;
            case 'create':
                $schemaManager->createDatabase($dbPath);
                $schemaTool = new SchemaTool($em);
                $schemaTool->createSchema($em->getMetadataFactory()->getAllMetadata());
                break;
            default:
                throw new \Exception("Invalid DB action \"{$action}\".");
        };
    }
}

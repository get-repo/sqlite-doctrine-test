<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/20718147/sqlite_logo.png" height=100 />
</p>

<h1 align=center>Symfony Kernel + Sqlite Testing</h1>
<h4 align=center style="border-bottom: 3px solid #008033; padding-bottom: 15px;">
    Test your Symfony app using Kernel and Sqlite database
</h4>

<br/>

## Table of Contents

1. [Installation](#installation)
1. [Usage](#usage)
    1. [registeredBundles](#registeredbundles)
    1. [registeredContainerConfiguration](#registeredcontainerconfiguration)
    1. [getDoctrineORMConfiguration](#getdoctrineormconfiguration)

<br/><br/>
## Installation

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/sqlite-doctrine-test git https://gitlab.com/get-repo/sqlite-doctrine-test.git
    composer require get-repo/sqlite-doctrine-test

<br/><br/>
## Usage

Create a Kernel class like this:
```php
<?php

namespace Test;

use GetRepo\SqliteDoctrineTest\AbstractKernel;

class Kernel extends AbstractKernel
{
}
```

### registeredBundles

Method to define bundles to register.
```php
    public function registeredBundles(): array
    {
        return [
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
        ];
    }
```
Note that the following bundles are always registered in
`GetRepo\SqliteDoctrineTest\AbstractKernel::registerBundles()`,
so no need to re-define them.

    new \Symfony\Bundle\FrameworkBundle\FrameworkBundle()
    new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle()

### registeredContainerConfiguration

Method to define custom services or parameters in the container.
```php
    public function registeredContainerConfiguration(ContainerBuilder $builder): void
    {
        // create your custom services or parameters
        $definition = new Definition();
        $definition->setClass(\App\My\Service::class);
        $definition->setPublic(true);
        $builder->setDefinition('app.my_service', $definition);
    }
```

### getDoctrineORMConfiguration

Method to define doctrine config, like:
https://symfony.com/doc/current/reference/configuration/doctrine.html

```php
    public function getDoctrineORMConfiguration(): array
    {
        return [
            'mappings' => [
                'src' => [
                    'type' => 'attribute',
                    'prefix' => 'App\Entity',
                    'dir' => __DIR__.'/src/App/Entity',
                ],
            ],
            // etc ...
        ];
    }
```


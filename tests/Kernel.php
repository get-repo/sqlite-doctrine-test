<?php

namespace Test;

use GetRepo\SqliteDoctrineTest\AbstractKernel;

class Kernel extends AbstractKernel
{
    protected function getDoctrineORMConfiguration(): array
    {
        return [
            'mappings' => [
                'tests' => [
                    'type' => 'attribute',
                    'prefix' => 'Test\Entity',
                    'dir' => __DIR__ . '/Entity',
                ],
            ],
        ];
    }
}

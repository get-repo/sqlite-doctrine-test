<?php

namespace Test;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use GetRepo\SqliteDoctrineTest\SqliteTestCase;
use Test\Entity\Test;

class SqliteTest extends SqliteTestCase
{
    public function testGetDoctrine(): void
    {
        $this->assertInstanceOf(ManagerRegistry::class, $this->getDoctrine());
    }

    public function testMetadatas(): void
    {
        $metadatas = $this->getDoctrine()
            ->getManager()
            ->getMetadataFactory()
            ->getAllMetadata();
        $this->assertIsArray($metadatas);
        $this->assertCount(1, $metadatas);
        $metadata = $metadatas[0];
        $this->assertInstanceOf(ClassMetadata::class, $metadata);
        /** @var \Doctrine\ORM\Mapping\ClassMetadata $metadata */
        $this->assertEquals(Test::class, $metadata->name);
    }
}
